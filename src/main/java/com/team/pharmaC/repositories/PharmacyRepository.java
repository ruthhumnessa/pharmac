package com.team.pharmaC.repositories;

import org.springframework.data.repository.CrudRepository;

import com.team.pharmaC.Pharmacy;

public interface PharmacyRepository extends CrudRepository<Pharmacy,Long> {

}
